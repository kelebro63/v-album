package ru.nineteam.virtualalbum.camera_api;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.hardware.Camera;
import android.net.Uri;
import android.view.SurfaceView;

/**
 * Created by User on 22.04.2014.
 */
public class MainCameraClass {

    private static MainCameraClass INSTANCE = null;
    private final int MAIN_CAMERA_ID = 0;

    private InitCamera initCamera;
    private Camera camera;
    private String albumName;
    private Uri mFileUri;
    private File photoFile;
    private int screenWidth;
    private int screenHeight;

    private MainCameraClass() {
    }

    public void initCamera(InitCamera initCamera, SurfaceView surface) {
        this.initCamera = initCamera;
        setScreenWidth(initCamera.getScreenWidth());
        setScreenHeight(initCamera.getScreenHeight());
        setCamera(this.initCamera.getCamera());
    }

    public void setPhotoFile() {
        photoFile = new File(initCamera.getStorageDir(), setFileName());
        mFileUri = Uri.fromFile(photoFile);
    }

    private String setFileName() {
        String timeStamp = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss")
                .format(new Date());
        String fileName = "VL_" + timeStamp + ".jpg";
        return fileName;
    }

    public Camera getCamera() {
        return camera;
    }

    public void setCamera(Camera camera) {
        this.camera = camera;
    }

    public static synchronized MainCameraClass getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new MainCameraClass();
        }
        return INSTANCE;
    }

    public String getAlbumName() {
        return albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public File getPhotoFile() {
        return photoFile;
    }

    public Uri getmFileUri() {
        return mFileUri;
    }

    public void setmFileUri(Uri mFileUri) {
        this.mFileUri = mFileUri;
    }

    public int getScreenHeight() {
        return screenHeight;
    }

    public void setScreenHeight(int screenHeight) {
        this.screenHeight = screenHeight;
    }

    public int getScreenWidth() {
        return screenWidth;
    }

    public void setScreenWidth(int screenWidth) {
        this.screenWidth = screenWidth;
    }


}
