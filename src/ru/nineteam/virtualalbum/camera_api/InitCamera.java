package ru.nineteam.virtualalbum.camera_api;

import java.io.File;
import java.util.List;

import android.content.Context;
import android.hardware.Camera;
import android.os.Environment;
import android.util.DisplayMetrics;

/**
 * Created by User on 22.04.2014.
 */
public class InitCamera {

    private File storageDir;
    private String albumName;
    private Camera camera;
    private int screenWidth;
    private int screenHeight;
    private int screenDpi;
    private DisplayMetrics metrics;
    private Camera.Parameters cameraParams;

    private final int PHOTO_QUALITY_PERSENT = 20;

	/*
	 * @name InitCamera
	 *
	 * @todo initializating camera
	 *
	 * @param context, cameraId
	 */

    public InitCamera(Context context, int cameraId) {
        camera = Camera.open(cameraId);
        cameraParams = camera.getParameters();
        List<String> focusModes = cameraParams.getSupportedFocusModes();
        if (focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO))
        {
            cameraParams.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        }
        cameraParams.setJpegQuality(PHOTO_QUALITY_PERSENT);
        camera.setParameters(cameraParams);
        metrics = context.getResources().getDisplayMetrics();
        screenWidth = metrics.widthPixels;
        screenHeight = metrics.heightPixels;
        screenDpi = metrics.densityDpi;
        setAlbumDir();
    }

    private void setAlbumDir() {
        setAlbumName("VirtualAlbum");
        storageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                getAlbumName());
        storageDir.mkdirs();
    }

    public Camera getCamera() {
        return camera;
    }


    public void setPreviewSize() {
        camera.getParameters().setPreviewSize(screenWidth, screenHeight);
    }

    public String getAlbumName() {
        return albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public File getStorageDir() {
        return storageDir;
    }

    public int getScreenWidth() {
        return screenWidth;
    }

    public int getScreenHeight() {
        return screenHeight;
    }

    public int getScreenDpi() {
        return screenDpi;
    }

}
