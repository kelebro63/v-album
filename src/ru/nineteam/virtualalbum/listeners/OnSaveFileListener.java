package ru.nineteam.virtualalbum.listeners;

import java.io.File;

public interface OnSaveFileListener {
	public void getSavedFile(File file);	
}
