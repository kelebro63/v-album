package ru.nineteam.virtualalbum.main_form;

import java.io.File;

import ru.nineteam.virtualalbum.PhotoUploader;
import ru.nineteam.virtualalbum.R;
import ru.nineteam.virtualalbum.listeners.OnSaveFileListener;
import ru.nineteam.virtualalbum.main_form.main_fragments.PhotoFragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Window;
import android.view.WindowManager;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.utils.StorageUtils;


/**
 * Created by User on 22.04.2014.
 */
public class MainActivity extends FragmentActivity implements OnSaveFileListener{
	
	private final int PHOTO_FRAGMENT = 0;
	
	private android.support.v4.app.FragmentManager frManager;
	public static final String ALBUM_CODE = "album code";
	private PhotoFragment mPhotoFragment;
	private int curentFragment;
	private ImageLoaderConfiguration mImageLoaderConfiguration;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		//TestFlight.takeOff(getApplication(), TEST_FLIGHT_ID);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.main_activity);
		mPhotoFragment = new PhotoFragment();
		mPhotoFragment.setOnSaveFileListener(this);
		initImageLoader();
		frManager = getSupportFragmentManager();
		frManager.beginTransaction().add(R.id.conteiner, mPhotoFragment)
				.commit();
		curentFragment = PHOTO_FRAGMENT;
	}
	
	private void initImageLoader() {
		File cacheDir = StorageUtils.getOwnCacheDirectory(getApplication(),
				"UniversalImageLoader/Cache");
		mImageLoaderConfiguration = new ImageLoaderConfiguration.Builder(
				getApplicationContext())
				.defaultDisplayImageOptions(
						new DisplayImageOptions.Builder()
								.showStubImage(R.drawable.cap)
								.showImageForEmptyUri(Color.RED)
								.cacheOnDisc(true)
								.imageScaleType(
										ImageScaleType.EXACTLY_STRETCHED)
								.build())
				.discCache(new UnlimitedDiscCache(cacheDir)).build();
		ImageLoader.getInstance().init(mImageLoaderConfiguration);
	}

	@Override
	public void getSavedFile(File file) {
		
		new PhotoUploader(this).execute(file);
	}


}
