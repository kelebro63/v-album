package ru.nineteam.virtualalbum.main_form.main_fragments;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import ru.nineteam.virtualalbum.R;
import ru.nineteam.virtualalbum.camera_api.InitCamera;
import ru.nineteam.virtualalbum.camera_api.MainCameraClass;
import ru.nineteam.virtualalbum.listeners.OnSaveFileListener;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.Size;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

public class PhotoFragment extends Fragment implements SurfaceHolder.Callback {

	private SurfaceView mSurface;
	private SurfaceHolder mSurfaceHolder;
	private final int CAMERA_ID = 0;
	private OnSaveFileListener mOnSaveFileListener = null;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.photo_fragment, null);
		mSurface = (SurfaceView) view.findViewById(R.id.surface);
		mSurfaceHolder = mSurface.getHolder();
		mSurfaceHolder.addCallback(this);

		MainCameraClass.getInstance().initCamera(
				new InitCamera(getActivity(), CAMERA_ID), mSurface);
		setCameraSurfaceSize();
		return view;
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		try {
			MainCameraClass.getInstance().getCamera().setPreviewDisplay(holder);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		MainCameraClass.getInstance().getCamera().startPreview();
		Handler mHandler = new Handler();
		mHandler.postDelayed(new Runnable() {

			@Override
			public void run() {
				makePhoto();
			}
		}, 4000);

	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {

	}

	@Override
	public void onPause() {
		super.onPause();
		if (MainCameraClass.getInstance().getCamera() != null)
			MainCameraClass.getInstance().getCamera().release();
		MainCameraClass.getInstance().setCamera(null);
	}

	public void resetPreview() {
		try {
			MainCameraClass.getInstance().getCamera()
					.setPreviewDisplay(mSurfaceHolder);
			MainCameraClass.getInstance().getCamera().startPreview();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void setCameraSurfaceSize() {
		int screenHeight = MainCameraClass.getInstance().getScreenHeight();
		Size previewCameraSize = MainCameraClass.getInstance().getCamera()
				.getParameters().getPreviewSize();
		mSurface.getLayoutParams().height = screenHeight;
		mSurface.getLayoutParams().width = previewCameraSize.width
				* screenHeight / previewCameraSize.height;
	}

	public void makePhoto() {
		MainCameraClass.getInstance().setPhotoFile();
		MainCameraClass.getInstance().getCamera()
				.takePicture(null, null, new PictureCallback() {

					@Override
					public void onPictureTaken(byte[] data, Camera camera) {
						resetPreview();
						new SaveFile().execute(data);
					}
				});
	}

	private class SaveFile extends AsyncTask<byte[], Void, File> {

		@Override
		protected File doInBackground(byte[]... params) {
			File writenFile = MainCameraClass.getInstance().getPhotoFile();
			try {
				FileOutputStream fos = new FileOutputStream(writenFile);
				try {
					fos.write(params[0]);
					fos.close();
				} catch (IOException e) {

					e.printStackTrace();
				}
			} catch (FileNotFoundException e) {

				e.printStackTrace();
			}
			return writenFile;
		}

		@Override
		protected void onPostExecute(File file) {
			mOnSaveFileListener.getSavedFile(file);
		}
	}

	public void setOnSaveFileListener(OnSaveFileListener listener) {
		mOnSaveFileListener = listener;
	}
}
