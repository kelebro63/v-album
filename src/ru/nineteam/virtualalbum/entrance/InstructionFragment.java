package ru.nineteam.virtualalbum.entrance;

import ru.nineteam.virtualalbum.R;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by User on 22.04.2014.
 */
public class InstructionFragment extends Fragment {

    private EditText etCodeField;
    private Button btnContinue;
    private View.OnClickListener onClickListener;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.instruction_fragment,null);
        etCodeField = (EditText)view.findViewById(R.id.etInstructionCode);
        btnContinue = (Button)view.findViewById(R.id.btnInstructionContinue);
        btnContinue.setOnClickListener(onClickListener);
        return view;
    }

    public void setOnClickListener(View.OnClickListener listener){
        onClickListener = listener;

    }

    public String getCode(){
        return etCodeField.getText().toString();
    }
}
