package ru.nineteam.virtualalbum.entrance;

import ru.nineteam.virtualalbum.R;
import ru.nineteam.virtualalbum.main_form.MainActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;


public class EntranceActivity extends ActionBarActivity implements Runnable, View.OnClickListener{

    private final int START_DELAY = 3000;
    private FragmentManager fragmentManager;
    private InstructionFragment instructionFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_entrance);
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().add(R.id.entranceFragmentContainer, new SplashFragment()).commit();
        instructionFragment = new InstructionFragment();
        instructionFragment.setOnClickListener(this);
        Handler handler = new Handler();
        handler.postDelayed(this, START_DELAY);
    }

    @Override
    public void run() {
        fragmentManager.beginTransaction().replace(R.id.entranceFragmentContainer, instructionFragment).commit();
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(MainActivity.ALBUM_CODE, instructionFragment.getCode());
        startActivity(intent);
        finish();
    }
}
