package ru.nineteam.virtualalbum;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by User on 22.04.2014.
 */
public class TextViewRoboto extends TextView{

    public TextViewRoboto(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (isInEditMode()) {
            return;
        }

        TypedArray styledAttrs = context.obtainStyledAttributes(attrs, R.styleable.TextViewRoboto);
        String fontName = styledAttrs.getString(R.styleable.TextViewRoboto_typeface);
        styledAttrs.recycle();

        if (fontName != null) {
            Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/"+ fontName);
            setTypeface(typeface);
        }
    }
}
